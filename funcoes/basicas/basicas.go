package main

import "fmt"

func F1() {
	fmt.Println("F1")
}

func f2(p1 string, p2 string) {
	fmt.Println(p1, p2)
}
func f3() string {
	return "F3"
}

func f4(p1, p2 string) string {
	return fmt.Sprintf("F4: %s %s", p1, p2)
}

func f5() (string, string) {
	return "Retorno 1", "Retorno 2"
}

func main() {
	F1()
	f2("er", "lang")
	f3()
	f4("eee", "eee")
	f5()
}

package main

import "fmt"

func main() {
	aprovados := []string{"Maria", "Pedro", "João"}
	imprimirAprovados(aprovados...)
}

func imprimirAprovados(aprovados, ...string) {
	fmt.Println("Lista de aprovados:")
	for i, aprovados := range aprovados {
		fmt.Printf("%d) %s\n", i+1, aprovados)
	}
}

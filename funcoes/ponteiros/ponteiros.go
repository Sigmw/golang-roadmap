package main

import "fmt"

func inc1(n int) {
	n++
}

// revisão: um ponteiro é representado por um *

func inc2(n *int) {
	// revisão: nesse caso o * está desreferenciando, para ter o valor que o ponteiro apontar
	*n++
}

func main() {
	n := 1

	inc1(n) // por valor, não por endereço de memória
	fmt.Println(n)
}

package main

import (
	"fmt"
	"strconv"
)

func main() {
	x := 2.4 // tipo float64
	y := 2
	// fmt.Println(y / x) não roda! precisam ser convertidos os valores
	fmt.Println(int(x) / y)
	fmt.Println(x / float64(y))

	nota := 6.9
	notaFinal := int(nota) //como se fosse um trunc
	fmt.Println(notaFinal)

	// cuidado..
	fmt.Println("Teste " + string(97)) // converteu pra utf-8, 97 é "a"

	// int pra string
	fmt.Println("Teste " + strconv.Itoa(123)) // string.Itoa é inteiro pra string


	// string pra int
	num, _ := strconv.Atoi("123")
	fmt.Println(num)

	// str to bool
	b, _ := strconv.ParseBool("true")
	if b {
		fmt.Println("Verdadeiro")
	}
}

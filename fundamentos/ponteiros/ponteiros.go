package main

import "fmt"

func main() {
	i := 1 // ocupa 64 bits na memória

	// Go não tem aritmética de ponteiros

	var p *int = nil
	p = &i // pegando o endereço da variável
	*p++   // desreferenciando o ponteiro p
	i++
	fmt.Println(p, i, *p, &i)

}

package main

import (
	"fmt"
	"math"
	"reflect"
)

func main() {
	//números inteiros
	fmt.Println(1, 2, 1000)
	fmt.Println("Literal inteiro de 32000 é", reflect.TypeOf(32000))

	// sem sinal (só positivos...) uint8 uint16 uint32 uint64
	var b byte = 255
	fmt.Println("O byte é", reflect.TypeOf(b))

	i1 := math.MaxInt64
	fmt.Println("O valor máximo do int no golang é de", i1)
	var i2 rune = 'a' // representação um mapeamento da tabela unicode (int32)
	fmt.Print("O rune de i2 é", reflect.TypeOf(i2))
	fmt.Print("e o valor é ", i2)

	//números reais (float32, float64)
	var x float32 = 49.99
	fmt.Println("O tipo do x é", reflect.TypeOf(x))
	fmt.Println("O tipo do literal 49.99 é", reflect.TypeOf(49.99))

	//boolean
	bo := true
	fmt.Println("O tipo de bo é", reflect.TypeOf(bo))
	fmt.Println(!bo)

	//string
	s1 := "Olá, meu nome é Lucas!"
	fmt.Println("O tipo de s1 é", reflect.TypeOf(s1))
	fmt.Println(s1 + "!!!!!!!!")
	fmt.Println("O tamanho de caracteres da string s1 é", len(s1))

	//string com multiplas linhas
	s2 := `Olá
	meu
	nome
	é
	Lucas`
	fmt.Println("O tamanho da string s2 é", len(s2))

	//char???
	char := 'a'
	fmt.Println("O tipo de char é", reflect.TypeOf(char))
	fmt.Println(char)
}

package main

import "fmt"

func main() {
	fmt.Printf("Outro programa em %s!\n", "Go")
}

/*
	"export PATH=$PATH/home/$USER/go/bin" exportar os comandos do 	go no .bashrc

	"godoc -http=:6060" Abre a documentação offline do go,

	"go vet" verifica código morto, construções suspeitas..
	ex: go vet comandos.go

	"go build" justamente faz a construção do código
	ex: go build comandos.go
*/

package main

import "fmt"

func main() {
	n1 := 0
	n2 := 0
	fmt.Print("Digite o primeiro valor da soma: ")
	fmt.Scanln(&n1)
	fmt.Print("Digite o segundo valor da soma: ")
	fmt.Scanln(&n2)
	resultado := somar(n1, n2)
	imprimir(resultado)
}
// go run *.go dentro do diretório funcoes
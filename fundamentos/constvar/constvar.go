package main

import (
	"fmt"
	m "math" // esse m diz que eu posso usar o math referenciando como m, veja ali na variável area (como no python: "import math as m")
)

func main() {
	const PI float64 = 3.1415
	var raio = 3.2 // tipo definido pelo próprio compilador (float64)

	area := PI * m.Pow(raio, 2) // o := declara e inicializa
	fmt.Println("A área da circunferência é:", area)

	const (
		a = 1
		b = 2
	)
	var (
		c = 3
		d = 4
	)
	fmt.Println(a, b, c, d)

	var e, f bool = true, false // bool = true vai pro e, false vai pro f
	fmt.Println(e,f)

	g, h, i := 2, false, "epa!" // g = 2 | h = false | i = "epa!"
	fmt.Println(g,h,i)
}

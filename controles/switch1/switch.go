package main

import "fmt"

func notaParaConceito(n float64) string {
	// o switch do golang faz o break automaticamente!
	var nota = int(n)
	switch nota {
	case 10:
		fallthrough // não fará nada, seguirá pro próximo case
	case 9:
		return "A"
	case 8, 7:
		return "B"
	case 6, 5:
		return "C"
	case 4, 3:
		return "D"
R	case 2, 1, 0:
		return "E"
	default:
		return "nenhuma das opções do case"
	}
}

func main() {
	fmt.Println(notaParaConceito(10))
}

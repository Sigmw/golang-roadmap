package main

import (
	"fmt"
)

func tipo(i interface{}) string {
	switch i.(type) {
	case int:
		return "Inteiro!"
	case float32, float64:
		return "Float, valor real!"
	case bool:
		return "Bool!"
	case string:
		return "String!"
	case func():
		return "Função"
	default:
		return "Sei não."
	}
}

func main() {
	fmt.Println(tipo(2.3))
	fmt.Println(tipo("a"))
	fmt.Println(tipo(1))
	fmt.Println(tipo(func() {}))
}

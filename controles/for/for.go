package main

import (
	"fmt"
	"time"
)

func main() {

	i := 1
	for i <= 10 {
		fmt.Println(i)
		i++
	}
	fmt.Println()
	for i := 0; i <= 20; i++ {
		fmt.Printf("%d ", i)
		if i%2 == 0 {
			fmt.Print("Par ")
		} else {
			fmt.Print("Impar ")
		}
	}
	i = 0
	for {
		// laço infinito aqui!
		fmt.Println(i)
		i++
		time.Sleep(time.Second)
	}
	//veremos o foreach no capítulo de array
}

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func numeroAleatorio() int {
	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)
	return r.Intn(10)
}

func main() {
	fmt.Println("Se o valor i sorteado de 0 até 10 for maior do que 5, você perde! \nAperte enter para receber o resultado...")
	a := 0
	fmt.Scanln(&a)
	if i := numeroAleatorio(); i > 5 {
		fmt.Println("Valor de i:",i)
		fmt.Println("Você perdeu, :/")
	} else { 
		fmt.Println("Valor de i:",i)
		fmt.Println("Boa, ganhou!!")
	}
}

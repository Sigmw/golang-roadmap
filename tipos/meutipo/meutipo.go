package main

type nota float64

func (n nota) entre(inicio, fim float64) bool {
	return float64(n) >= inicio && float64/(n) <= fim
}
